const mongoose = require("mongoose");
const bcrypt = require('bcrypt');

const deckModel = mongoose.model(
  'deck',
  mongoose.Schema(
      {
          user_Id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user'
          },
          title: {
            type: String
          },
          category_Id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'category'
          },
          description: {
            type: String
          },
          color: {
            type: String
          },
      },
      { timestamps: false }
  )
)

// const newDeck = new deckModel({
//     user_Id: "60d60f8fbbca59547a5fe12c",
//     title: "Machine Learning",
//     category_Id: "60dad5e3ec793968c5ea498d",
//     description: "Penjelasan Machine Learning",
//     color: "#AB6FDC"
// })
// newDeck.save()


module.exports = deckModel;