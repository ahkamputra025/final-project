const mongoose = require("mongoose");

const practiceTestModel = mongoose.model(
  'practiceTest',
  mongoose.Schema(
      {
        decksDetails_Id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'decksDetails'
        },
        card_Id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'card'
        },
        status: {
            type: String
        },
        type: {
            type: String
        }
      },
      { timestamps: false }
  )
)

module.exports = practiceTestModel;