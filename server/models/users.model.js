const mongoose = require("mongoose");
const bcrypt = require('bcrypt');

const userModel = mongoose.model(
  'user',
  mongoose.Schema(
      {
          email: {
            type: String,
          },
          password: {
            type: String,
            set: encryptPassword
          },
          username: {
            type: String
          }
      },
      { timestamps: false }
  )
)

function encryptPassword(password){
  const encryptPassword = bcrypt.hashSync(password, 10);
  return encryptPassword;
}

module.exports = userModel;