const User = require('./users.model');
const Category = require('./category.model');
const Deck = require('./decks.model');
const Card = require('./card.model');
const DecksDetails = require('./decksDetails.model');
const CardExpal = require('./cardExpal.model');
const DecksTest = require('./decksTest.model');
const TakeTest = require('./take_test.model');
const PracticeTest = require('./practiceTest.model');

module.exports = { 
    User, 
    Category, 
    Deck, 
    Card, 
    DecksDetails,
    CardExpal,
    DecksTest,
    TakeTest,
    PracticeTest
};
