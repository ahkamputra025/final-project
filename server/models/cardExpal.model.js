const mongoose = require("mongoose");

const cardExpal = mongoose.model(
  'cardExpal',
  mongoose.Schema(
      {
        cardId: {
            type: String,
            ref: 'card'
        },
        explane1: {
            type: String
        },
        explane2: {
            type: String
        },
        explane3: {
            type: String
        },
        explane4: {
            type: String
        },
        status: {
            type: Boolean
        }
      },
      { timestamps: false }
  )
)

module.exports = cardExpal;