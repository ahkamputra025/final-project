const mongoose = require("mongoose");

const takeTest = mongoose.model(
  'takeTest',
  mongoose.Schema(
      {
        decksTest_Id: {
            type: mongoose.Schema.Types.ObjectId
        },
        cardId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'card'
        },
        type: {
            type: Boolean
        }
      },
      { timestamps: false }
  )
)

module.exports = takeTest;