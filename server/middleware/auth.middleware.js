const joi = require("joi");
const jwt = require('jsonwebtoken');
const method = require("../controllers/profile.controller");

const authMiddleware = {};


authMiddleware.checkToken = (req, res, next) => {
    const token = req.header('Authorization');
    if(!token) return res.status(401).json({message: 'Failed to authenticate token'});

    try {
        const verified = jwt.verify(token, process.env.SECRET_KEY);
        req.token = verified;
        next();
    } catch (error) {
        res.status(400).json({message: 'No token provided.'});
    }
}

authMiddleware.ValidateRegister = async (req, res, next) => {
    const regisSchema = joi.object({
        email: joi.string().email(),
        password: joi.string(),
        username: joi.string().allow('')
    });

    const {error}  = await regisSchema.validate(req.body);
    
    if(error){
        res.status(400).send({
            status: 400,
            message: error.details[0].message,
          });
    } else {
        next();
    }
}

authMiddleware.ValidateLogin = async (req, res, next) => {
    const loginSchema = joi.object({
        email: joi.string().email(),
        password: joi.string(),
    });

    const {error}  = await loginSchema.validate(req.body);
    
    if(error){
        res.status(400).send({
            status: 400,
            message: error.details[0].message,
          });
    } else {
        next();
    }
}

authMiddleware.validateProfile = async (req, res, next) => {
    const profileSchema = joi.object({
        username: joi.string(),
        email: joi.string().email(),
    });

    const { error } = await profileSchema.validate(req.body);

    if(error){
        res.status(400).send({
            status: 400,
            message: error.details[0].message,
          });
    } else {
        next();
    }
}

authMiddleware.validatePassword = async (req, res, next) => {
    const passwordSchema = joi.object({
        current_password: joi.string(),
        password: joi.string(),
    });

    const { error } = await passwordSchema.validate(req.body);

    if(error){
        res.status(400).send({
            status: 400,
            message: error.details[0].message,
          });
    } else {
        next();
    }
}

authMiddleware.validateCreateorUpdateDeck = async (req, res, next) => {
    const createDeckSchema = joi.object({
        title: joi.string(),
        category_Id: joi.string(),
        description: joi.string().allow(''),
        color: joi.string(),
    });

    const { error } = await createDeckSchema.validate(req.body);

    if(error){
        res.status(400).send({
            status: 400,
            message: error.details[0].message,
          });
    } else {
        next();
    }
}

authMiddleware.validateCreateorUpdateCard = async (req, res, next) => {
    const createorUpdateCardSchema = joi.object({
        decksId: joi.string(),
        term: joi.string(),
        explanation1: joi.string(),
        explanation2: joi.string(),
        explanation3: joi.string(),
        explanation4: joi.string(),
    });

    const { error } = await createorUpdateCardSchema.validate(req.body);

    if(error){
        res.status(400).send({
            status: 400,
            message: error.details[0].message,
          });
    } else {
        next();
    }
}

module.exports = authMiddleware;