const method = {}
const {Card, CardExpal, Deck} = require('../models')
const fs = require('fs')
const cloudinary = require('../service/cloudinary')

method.getAllCard = async (req, res) => {
    try{
        const findCards = await Card.find()

        res.status(200).send({
            statusCode: 200,
            statusText: "success",
            message: " Your request for Get All Cards successfully",
            data: findCards
        });

    }catch(error){
        res.status(500).send(error);
    }
}

method.getCardId = async (req, res) => {
    try{        

        const decksId = req.params.decksId;
        const findCard = await Card.find({ decksId: decksId});
        const IdCard = await findCard.map((x) => x._id);
        const findDeck = await Deck.findOne({_id: decksId})

        if(findDeck == null){
            res.status(404).send({ message: "Decks Id does not exist" });
        }else {

            var penampung1 = [];
            for(let i=0; i<IdCard.length; i++){
                penampung1.push( await Card.find({ _id: IdCard[i]}).select('-explanation'));
                const dataSplit = await penampung1[i].map((x) => x.imageExplanation).toString()
 
                    penampung1[i].push({
                        type: null
                    })
                    penampung1[i].push( await CardExpal.find({ cardId: IdCard[i]}).select('-cardId').select('-_id').select('-__v')); 
                
            }
            
            res.status(200).json({
                statusCode: 200,
                statusText: "success",
                message: " Your request for Get Card By DecksId successfully",
                data: penampung1
            });
        
        }
    }catch(error){
        res.status(500).send(error);
    }
}


method.createCard = async (req, res) => {
    try{
        const {decksId, term, explanation1, explanation2, explanation3, explanation4} = req.body
        const file = req.file
        if(file === undefined){
            const image = null
            const createCard = new Card({
                decksId: decksId,
                term: term,
                imageExplanation: image,
                explanation: explanation1,
            })
            const saveCard = await createCard.save()
            const idCard = createCard._id

            //create Wrong Answer
            const createExplane1 = new CardExpal({
                cardId: idCard, 
                explane1: explanation1, 
                status: true
            });
        
            const createExplane2 = new CardExpal({
                cardId: idCard, 
                explane2: explanation2,
                status: false
            });
        
            const createExplane3 = new CardExpal({
                cardId: idCard, 
                explane3: explanation3,
                status: false
            });
        
            const createExplane4 = new CardExpal({
                cardId: idCard, 
                explane4: explanation4,
                status: false
            });
        
            await createExplane1.save();
            await createExplane2.save();
            await createExplane3.save();
            await createExplane4.save();

            res.send({
                statusCode: 200,
                statusText: "success",
                message: " Your request for Create Card successfully",
                data: {
                    Card: saveCard,
                    Explanation2: explanation2,
                    Explanation3: explanation3,
                    Explanation4: explanation4
                }
            })

        }else {
            const uploader = async (path) => await cloudinary.uploads(path,'Images')

            const {path} = file
            const newPath = await uploader(path)
            const image = newPath.url
            fs.unlinkSync(path)

            const createCard = new Card({
                decksId: decksId,
                term: term,
                imageExplanation: image,
                explanation: explanation1,
            })
            const saveCard = await createCard.save()
            const idCard = createCard._id

            //create Wrong Answer
            const createExplane1 = new CardExpal({
                cardId: idCard, 
                explane1: explanation1, 
                status: true
            });
        
            const createExplane2 = new CardExpal({
                cardId: idCard, 
                explane2: explanation2,
                status: false
            });
        
            const createExplane3 = new CardExpal({
                cardId: idCard, 
                explane3: explanation3,
                status: false
            });
        
            const createExplane4 = new CardExpal({
                cardId: idCard, 
                explane4: explanation4,
                status: false
            });
        
            await createExplane1.save();
            await createExplane2.save();
            await createExplane3.save();
            await createExplane4.save();

            res.send({
                statusCode: 200,
                statusText: "success",
                message: " Your request for Create Card successfully",
                data: {
                    Card: saveCard,
                    Explanation2: explanation2,
                    Explanation3: explanation3,
                    Explanation4: explanation4
                }
            })
        }
    }catch(error){
        res.status(500).send(error);
    }
}

method.updateCard = async (req, res) => {
    try{
        const id = req.params.id
        const dtCard = await Card.findById({_id : id})
        const {decksId, urlImage, term, explanation1, explanation2, explanation3, explanation4} = req.body
        const file = req.file
        const dtCardExplane = await CardExpal.find({cardId: id})
        const idExplane = await dtCardExplane.map((x) => x._id)
        const explane1 = await CardExpal.findById({_id: idExplane[0]})
        const explane2 = await CardExpal.findById({_id: idExplane[1]})
        const explane3 = await CardExpal.findById({_id: idExplane[2]})
        const explane4 = await CardExpal.findById({_id: idExplane[3]})
        
        if(dtCard){

            if(!urlImage){
                // res.send('Oke');

                if(file === undefined){
                const image = null
                const dtUpdate = {
                    decksId: decksId,
                    term: term,
                    imageExplanation: image,
                    explanation: explanation1
                };

                dtCard.set(dtUpdate);
                const result = await dtCard.save();

                const updateExplane1 ={
                    cardId: id, 
                    explane1: explanation1, 
                    status: true
                }

                const updateExplane2 ={
                    cardId: id, 
                    explane2: explanation2,
                    status: false
                }

                const updateExplane3 ={
                    cardId: id, 
                    explane3: explanation3,
                    status: false
                }

                const updateExplane4 ={
                    cardId: id, 
                    explane4: explanation4,
                    status: false
                }

                explane1.set(updateExplane1);
                const resultExplane1 = await explane1.save();
                explane2.set(updateExplane2);
                const resultExplane2 = await explane2.save();
                explane3.set(updateExplane3);
                const resultExplane3 = await explane3.save();
                explane4.set(updateExplane4);
                const resultExplane4 = await explane4.save();
                
                res.status(200).send({
                    statusCode: 200,
                    statusText: "success",
                    message: " Update Card Succesfully ",
                    data: {
                        Card: result,
                        Explanation2: explanation2,
                        Explanation3: explanation3,
                        Explanation4: explanation4
                    }
                  });            
            } else {
                const uploader = async (path) => await cloudinary.uploads(path,'Images')

                const {path} = file
                const newPath = await uploader(path)
                const image = newPath.url
                fs.unlinkSync(path)
                
                const dtUpdate = {
                    decksId: decksId,
                    term: term,
                    imageExplanation: image,
                    explanation: explanation1,
                    
                };

                dtCard.set(dtUpdate);
                const resultCard = await dtCard.save();

                const updateExplane1 ={
                    cardId: id, 
                    explane1: explanation1, 
                    status: true
                }

                const updateExplane2 ={
                    cardId: id, 
                    explane2: explanation2,
                    status: false
                }

                const updateExplane3 ={
                    cardId: id, 
                    explane3: explanation3,
                    status: false
                }

                const updateExplane4 ={
                    cardId: id, 
                    explane4: explanation4,
                    status: false
                }

                explane1.set(updateExplane1);
                const resultExplane1 = await explane1.save();
                explane2.set(updateExplane2);
                const resultExplane2 = await explane2.save();
                explane3.set(updateExplane3);
                const resultExplane3 = await explane3.save();
                explane4.set(updateExplane4);
                const resultExplane4 = await explane4.save();

                res.status(200).send({
                    statusCode: 200,
                    statusText: "success",
                    message: " Update Card Succesfully ",
                    data: {
                        Card: resultCard,
                        Explanation2: explanation2,
                        Explanation3: explanation3,
                        Explanation4: explanation4
                    }
                  });           
            }

            } else {
                // res.send('Not Oke');

                if(file === undefined){
                    const image = null
                    const dtUpdate = {
                        decksId: decksId,
                        term: term,
                        imageExplanation: urlImage,
                        explanation: explanation1
                    };
    
                    dtCard.set(dtUpdate);
                    const result = await dtCard.save();
    
                    const updateExplane1 ={
                        cardId: id, 
                        explane1: explanation1, 
                        status: true
                    }
    
                    const updateExplane2 ={
                        cardId: id, 
                        explane2: explanation2,
                        status: false
                    }
    
                    const updateExplane3 ={
                        cardId: id, 
                        explane3: explanation3,
                        status: false
                    }
    
                    const updateExplane4 ={
                        cardId: id, 
                        explane4: explanation4,
                        status: false
                    }
    
                    explane1.set(updateExplane1);
                    const resultExplane1 = await explane1.save();
                    explane2.set(updateExplane2);
                    const resultExplane2 = await explane2.save();
                    explane3.set(updateExplane3);
                    const resultExplane3 = await explane3.save();
                    explane4.set(updateExplane4);
                    const resultExplane4 = await explane4.save();
                    
                    res.status(200).send({
                        statusCode: 200,
                        statusText: "success",
                        message: " Update Card Succesfully ",
                        data: {
                            Card: result,
                            Explanation2: explanation2,
                            Explanation3: explanation3,
                            Explanation4: explanation4
                        }
                      });            
                } else {
                    const uploader = async (path) => await cloudinary.uploads(path,'Images')
    
                    const {path} = file
                    const newPath = await uploader(path)
                    const image = newPath.url
                    fs.unlinkSync(path)
                    
                    const dtUpdate = {
                        decksId: decksId,
                        term: term,
                        imageExplanation: image,
                        explanation: explanation1,
                        
                    };
    
                    dtCard.set(dtUpdate);
                    const resultCard = await dtCard.save();
    
                    const updateExplane1 ={
                        cardId: id, 
                        explane1: explanation1, 
                        status: true
                    }
    
                    const updateExplane2 ={
                        cardId: id, 
                        explane2: explanation2,
                        status: false
                    }
    
                    const updateExplane3 ={
                        cardId: id, 
                        explane3: explanation3,
                        status: false
                    }
    
                    const updateExplane4 ={
                        cardId: id, 
                        explane4: explanation4,
                        status: false
                    }
    
                    explane1.set(updateExplane1);
                    const resultExplane1 = await explane1.save();
                    explane2.set(updateExplane2);
                    const resultExplane2 = await explane2.save();
                    explane3.set(updateExplane3);
                    const resultExplane3 = await explane3.save();
                    explane4.set(updateExplane4);
                    const resultExplane4 = await explane4.save();
    
                    res.status(200).send({
                        statusCode: 200,
                        statusText: "success",
                        message: " Update Card Succesfully ",
                        data: {
                            Card: resultCard,
                            Explanation2: explanation2,
                            Explanation3: explanation3,
                            Explanation4: explanation4
                        }
                      });           
                }

            }
        } else {
            res.status(500).send({ message: "id does not exist" });
        }

    }catch(error){
        res.status(500).send(error);
    }
}


method.deleteCard = async (req, res) => {
    try{
        const checkId = await Card.findById(req.params.id);
        if (checkId) {
            await Card.deleteOne({ _id: req.params.id });
            await CardExpal.deleteMany({cardId: req.params.id})
            res.status(200).send({
              statusCode: 200,
              statusText: "success",
              message: " Card Was Deleted Succesfully ",
            });
        }else {
            res.status(500).send({ message: "id does not exist" });
        }
      
    }catch(error){
        res.status(500).send(error);
    }
}

module.exports = method

