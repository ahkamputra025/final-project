const { DecksDetails, Card, Category, Deck } = require('../models');
const method = {};

method.getdecksDetailsByIdDecks = async (req, res) => {
    try {
        const ID  = req.params.id;
        const token = req.token;
        const idUser = token.id;

        const dtDecks = await Deck.find({ _id: ID });
        const dt = await dtDecks.map((x) => x.category_Id);
        const title = await dtDecks.map((x) => x.title);
        const description = await dtDecks.map((x) => x.description);
        const dtCategory = await Category.find({ _id: dt });
        const label = await dtCategory.map((z) => z.label);
        const totalTerm = await Card.find({ decksId: ID}).countDocuments();
        let dtDecksDetailsByIdDecks = await DecksDetails.find({ decksId: ID, userId: idUser });
        if(dtDecksDetailsByIdDecks[0] == undefined){
            const totalNotStudied = totalTerm - 0
            res.status(200).send({
                statusCode: 200,
                statusText: "success",
                message: " Your request for Get Decks details by decks id successfully",
                data: {title, totalTerm, label, description ,totalNotStudied, dtDecksDetailsByIdDecks }
            });

        }else {
            const trueScore = await dtDecksDetailsByIdDecks.map((x) => x.true);
            const falseScore = await dtDecksDetailsByIdDecks.map((x) => x.false);
            const tambah = Number(trueScore) + Number(falseScore)
            const totalNotStudied = totalTerm - tambah


            res.status(200).send({
                statusCode: 200,
                statusText: "success",
                message: " Your request for Get Decks details by decks id successfully",
                data: {title, totalTerm, label, description, totalNotStudied, dtDecksDetailsByIdDecks }
            });
        }

    } catch (error) {
        res.status(500).send(error);
    }
}

method.getDecksDetailsAllByUserID = async (req, res) => {
    try {
        const token = req.token;
        const idUser = token.id;
        
        const dtDecksDetails = await DecksDetails.find({ userId: idUser}).populate('decksId').limit(5);

        res.status(200).send({
            statusCode: 200,
            statusText: "success",
            message: " Your request for Get All Decks details by user id successfully",
            data: dtDecksDetails
        });

    } catch (error) {
        res.status(500).send(error);
    }
}

method.updateDecksDetails = async (req, res) => {
    try{

    }catch(error){
        
    }
}

method.createDecksDetails = async (req, res) => {
    try {
        const { userId, decksId } = req.body;

        const dtDecksDetails = new DecksDetails(
            {
                userId: userId,
                decksId: decksId,
                progress: 0,
                true: 0,
                false: 0
            }
        );

        const result = await dtDecksDetails.save();

        res.status(200).send({
            statusCode: 200,
            statusText: "success",
            message: " Your request for Create DecksDetails successfully",
            data: result
        });

    } catch (error) {
        res.status(500).send(error);
    }
}

method.putDecksDetails = async (req, res) => {
    const { userId, decksId, True, False} = req.body;

    const dtDecksDetails = await DecksDetails.findById(req.params.id);

    if(dtDecksDetails){

        const totalCardDecksId = await Card.find({ decksId: decksId}).countDocuments();
        const progress = (True + False) * 100 / totalCardDecksId;
        
        const dtUpdate = {
            userId: userId,
            decksId: decksId,
            progress: progress,
            true: True,
            false: False
        }
        
        dtDecksDetails.set(dtUpdate);
        const result = await dtDecksDetails.save();

        res.status(200).json({
            statusCode: 200,
            statusText: "success",
            message: " Your request for update decksDetails successfully",
            data: result
        });
    }
}

module.exports = method;