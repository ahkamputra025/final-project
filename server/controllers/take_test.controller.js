const { DecksTest, TakeTest } = require('../models');
const method = {};

method.getResultTakeTest = async (req, res) => {
    try {
        const token = req.token;
        const idUser = token.id;
        
        const dtResultDecksTest = await DecksTest.find({ userId: idUser, deckId: req.params.id });
        const dtIdDecksTest = await dtResultDecksTest.map((x) => x._id);

        const dtResultTakeTest = await TakeTest.find({ decksTest_Id: dtIdDecksTest }).populate('cardId');

        res.status(200).json({
            statusCode: 200,
            statusText: 'success',
            message: " Your request get result take a test successfully",
            data: {dtResultDecksTest, dtResultTakeTest}
        });

    } catch (error) {
        res.status(500).send(error);
    }
}

method.RetakeTheTest = async (req, res) => {
    try {
        const token = req.token;
        const idUser = token.id;

        const dtDecksTest = await DecksTest.find({ userId: idUser, deckId: req.params.id });
        const IdDtDecksTest = await dtDecksTest.map((x) => x._id );

        const updateDtDecksTest = await DecksTest.findById(IdDtDecksTest);
        const updateDecksTest = {
            score: 0,
            true: 0,
            false: 0
        };
        updateDtDecksTest.set(updateDecksTest);
        await updateDtDecksTest.save();

        await TakeTest.deleteMany({ decksTest_Id: IdDtDecksTest });

        res.status(200).send({
            statusCode: 200,
            statusText: "success",
            message: "Retake the test Succesfully "
        });

    } catch (error) {
        res.status(500).send(error);
    }
}

module.exports = method;