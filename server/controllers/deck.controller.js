const { Deck, Category, User, DecksDetails } = require("../models");
const method = {};

method.getSearchByIdCategory = async (req, res) => {
  const { category = [] } = req.body;
  
 try{

  const result = await Deck.find({ category_Id: { $in: category  }}).populate('user_Id');

  if(result[0] === undefined ){
    res.status(404).send({ message: `No search results found :(` });
  }else{
    res.send({
      statusCode: 200,
      statusText: "success",
      message: " Your request for Get All Decks By Category ID successfully",
      data: result
    })
  }

 }catch(error){
  e.message = `Destination Service, Search by category failed: ${e.message}`;
  throw e;
 }
};

method.getDeckByUserId = async(req, res) => {
  try{
    const token = req.token;
    const idUser = token.id;
    const getDeck = await Deck.find({user_Id: idUser}).populate('user_Id', 'username')
    if(getDeck[0] == undefined){
      res.status(404).send({ message: `No search results found :(` });
    }else{
      res.send({
        statusCode: 200,
        statusText: "success",
        message: " Your request for Get All Decks By Category ID successfully",
        data: getDeck
      })
    }

  }catch(error){
    e.message = `Destination Service, Search by category failed: ${e.message}`;
    throw e;
  }
}

method.sortDeck = async (req, res) => {
  try{
    const sortData = req.body.sort
    const token = req.token;
    const idUser = token.id;
    
    if(sortData == 1){
      let data = []
        const decksUser = await Deck.find().sort({_id: -1})
        data.push(decksUser)
        
          res.send({
            statusCode: 200,
            statusText: "success",
            message: " Your request for Get All Decks By Category ID successfully",
            data: data
          })
    } else if(sortData == 2){
      const sortTrue = {true: -1};
      const decksDetail = await DecksDetails.find({userId: idUser}).sort(sortTrue)
      const returnDecks = await Deck.find()
      let data = []
      if(decksDetail[0] == undefined){
        data.push(returnDecks)

        res.send({
          statusCode: 200,
          statusText: "success",
          message: " Your request for Get All Decks where you don't have studied in this decks successfully",
          data: data
        })  
      } else{
        const dt = await decksDetail.map((x) => `${x.decksId}`);
        const dtDecksNext = await Deck.find({_id: {$nin: dt}})
        const result = dt.map( x => returnDecks.filter(z => z._id == x))
        dtDecksNext.map((x) => result[0].push(x))
        res.send({
          statusCode: 200,
          statusText: "success",
          message: " Your request for Get All Decks where result practice Most mastered successfully",
          data: result
        })
      }
    } else if(sortData == 3){
      const sortFalse = {false: -1};
      const decksDetail = await DecksDetails.find({userId: idUser}).sort(sortFalse)
      const returnDecks = await Deck.find()
      let data = []
      if(decksDetail[0] == undefined){
        data.push(returnDecks)

        res.send({
          statusCode: 200,
          statusText: "success",
          message: " Your request for Get All Decks where you don't have studied in this decks successfully",
          data: data
        })   
      } else{
        const dt = await decksDetail.map((x) => `${x.decksId}`);
        const dtDecksNext = await Deck.find({_id: {$nin: dt}})
        const result = dt.map( x => returnDecks.filter(z => z._id == x))
        dtDecksNext.map((x) => result[0].push(x))
        res.send({
          statusCode: 200,
          statusText: "success",
          message: " Your request for Get All Decks where result practice Most need to recall successfully",
          data: result
        })
      }
    }else{
      const sortFalse = {notStudied: 1};
      const decksDetail = await DecksDetails.find({userId: idUser}).sort(sortFalse)
      const find = await Deck.find()
      let data = []
      if(decksDetail[0] == undefined){
        
        data.push(find)
        res.send({
          statusCode: 200,
          statusText: "success",
          message: " Your request for Get All Decks where you don't have studied in this decks successfully",
          data: data
        })   
      }else{
        const decksId = await decksDetail.map((x) => `${x.decksId}`);
        const result = decksId.map( x => find.filter(z => z._id == x))
        const returnDecks = await Deck.find({_id: {$nin: decksId}})
        result[0].map((x) => returnDecks.push(x))
        data.push(returnDecks)
        res.send({
          statusCode: 200,
          statusText: "success",
          message: " Your request for Get All Decks where you don't have studied in this decks successfully",
          data: data
        })   
      }
    }
  }catch(error){
    res.status(500).send(error);
  }
}

method.getCategory = async (req, res) => {
  try{
    const getCategory = await Category.find()

    res.send({
      statusCode: 200,
      statusText: "success",
      message: " Your request for Get All Decks By Category ID successfully",
      data: getCategory
    })

  }catch(error){
    res.status(500).send(error);
  }
}

method.getDeckAll = async (req, res) => {
    try {
        const NUMBER_OF_ITEMS = 12;
        const PAGE_NUMBER = req.params.id;

        const dtDecks = await Deck.find().sort({_id: -1}).populate('user_Id', 'username').populate('category_Id').skip(NUMBER_OF_ITEMS * (PAGE_NUMBER - 1)).limit(NUMBER_OF_ITEMS)
        
        if(dtDecks[0] == undefined){
            res.status(200).send({
              statusCode: 200,
              statusText: "success",
              message: " Your request for Get All Decks finished",
            });
        } else {
            res.status(200).send({
              statusCode: 200,
              statusText: "success",
              message: " Your request for Get All Decks successfully",
              data: dtDecks
          });
        }

    } catch (error) {
        res.status(500).send(error);
    }
}

method.getDeckLimit = async (req, res) => {
    try {
        const token = req.token;
        const idUser = token.id;
        const dtDecks = await Deck.find({user_Id: idUser}).sort({_id: -1}).limit(4);
        
        if(dtDecks[0] == undefined){
          res.status(200).send({
            statusCode: 200,
            statusText: "success",
            message: "You don't have decks :(",
          });
        }else{
          res.status(200).send({
              statusCode: 200,
              statusText: "success",
              message: " Your request for Get Decks successfully",
              data: dtDecks
          });
       } 

    } catch (error) {
        res.status(500).send(error);
    }
}

method.getAllDecks = async (req, res) => {
    try{
        const decks = await Deck.find().populate('user_Id').populate('category_Id');
        
        res.send({
            statusCode: 200,
            statusText: "success",
            message: " Your request for Get All Decks successfully",
            data: decks
        })
    }catch(error){
        res.status(500).send(error);
    }
}


method.createDeck = async (req, res) => {
    try{
        const {title, category_Id, description, color} = req.body
        const token = req.token;
        const idUser = token.id;
        const createDeck = new Deck({
            user_Id: idUser,
            title: title,
            category_Id: category_Id,
            description: description,
            color: color
        })
        const saveDeck = await createDeck.save()
        
        res.send({
            statusCode: 200,
            statusText: "success",
            message: " Your request for Create Decks successfully",
            data: saveDeck
        })
    }catch(error){
        res.status(500).send(error);
    }
}

method.findDecksId = async (req, res) => {
  try{
      const id = req.params.id
      const token = req.token;
      const idUser = token.id;
      const findDeck = await Deck.findOne({_id: id, user_Id: idUser})

      if(findDeck == undefined){
        res.status(404).send({ message: "you don't have not access" });
      }else{
        res.status(200).send({
          statusCode: 200,
          statusText: "success",
          message: " Your request for Get All Cards successfully",
          data: findDeck
        });
      }

  }catch(error){
      res.status(500).send(error);
  }
}


method.updateDeck = async (req, res) => {
  try {
    const token = req.token;
    const idUser = token.id;
    const dtDecks = await Deck.findOne( {_id: req.params.id, user_Id: idUser});

    if (dtDecks) {
      const dtUpdate = {
        title: req.body.title,
        user_Id: idUser,
        category_Id: req.body.category_Id,
        description: req.body.description,
        color: req.body.color,
      };

      dtDecks.set(dtUpdate);
      const result = await dtDecks.save();
      res.status(200).send({
        statusCode: 200,
        statusText: "success",
        message: " Update Deck Succesfully ",
        data: result,
      });
    }else {
        res.status(404).send({ message: "you don't have not access" });
      }
  } catch (error) {
    res.status(500).send(error);
  }
};

method.deleteDecks = async (req, res) => {
  try {
    const token = req.token;
    const idUser = token.id;
    const checkId = await Deck.findOne({ _id: req.params.id, user_Id: idUser });
    if (checkId) {
      const searchDetails = await DecksDetails.find({decksId: req.params.id})
      const idDecksDetails = await searchDetails.map((x) => x._id)
      const searchCard = await Card.find({decksId: req.params.id})
      const idCard = await searchCard.map((x) => x.id)
      await PracticeTest.deleteMany({decksDetails_Id: {$in: idDecksDetails}})
      await DecksDetails.deleteMany({decksId: req.params.id})
      await Deck.deleteOne({ _id: req.params.id, user_Id: idUser });
      await Card.deleteMany({ _id: {$in: idCard} });
      await CardExpal.deleteMany({cardId: {$in: idCard}})
      res.status(200).send({
        statusCode: 200,
        statusText: "success",
        message: " Deck Was Deleted Succesfully ",
      });
    } else {
      res.status(500).send({ message: "id does not exist" });
    }
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = method;
