const { Deck, Category, User, DecksDetails, PracticeTest, Card } = require("../models");
const method = {};

method.practiceTest = async (req, res) => {
    try{
        const token = req.token;
        const idUser = token.id;
        const idDecks = req.params.id
        const {next} = req.body;
        const convToNumber = Number(next);

        const dtCard = await Card.find({ decksId: idDecks}).skip(convToNumber).limit(1);

        const totalCardDecksId = await Card.find({ decksId: idDecks}).countDocuments();
        const searchDecksDetails = await DecksDetails.findOne({decksId: idDecks, userId: idUser}).select('-notStudied')
        
        //create decks details kalau belum
        if(searchDecksDetails === null){
            const dtDecksDetails = new DecksDetails(
                {
                    userId: idUser,
                    decksId: idDecks,
                    progress: 0,
                    true: 0,
                    false: 0,
                    notStudied: totalCardDecksId

                }
            );
            const result = await dtDecksDetails.save();
            const id = dtDecksDetails._id
            const findDecksDetails = await DecksDetails.findOne({_id: id}).select('-notStudied')

            const dtTestPractice = await Card.find({decksId: idDecks})
            res.status(200).json({
                statusCode: 200,
                statusText: 'success',
                message: " Your testing successfully",
                data:{
                    TotalCard: totalCardDecksId,
                    TotalNotStudied: totalCardDecksId,
                    DecksDetails: findDecksDetails,
                    TestPractice: dtTestPractice
                }
            });
        } else {
            const idDecksDet = searchDecksDetails._id
            const dtPracticeTest = await PracticeTest.find({decksDetails_Id: idDecksDet})
            const idCard = await dtPracticeTest.map((x) => `${x.card_Id}`)
            const returnDecks = await Card.find({_id: {$nin: idCard}, decksId: idDecks}).countDocuments()
            const dtCardId = await Card.find({_id: {$nin: idCard}, decksId: idDecks})
            const notStudied =await Card.find({_id: {$nin: idCard}, decksId: idDecks}).countDocuments()
            const find = await Card.find({decksId: idDecks}).countDocuments()

            res.status(200).json({
                statusCode: 200,
                statusText: 'success',
                message: " Your testing successfully",
                data:{
                    TotalCard: totalCardDecksId,
                    TotalNotStudied: notStudied,
                    DecksDetails: searchDecksDetails,
                    TestPractice: dtCardId
                }
            });
            }
    }catch(error){
        res.status(500).send(error);
    }
}

method.swipeLeft = async (req, res) => {
    try{
        const token = req.token;
        const idUser = token.id;
        const {decksDetailsId, cardId} = req.body

        const searchPractice = await PracticeTest.findOne({decksDetails_Id: decksDetailsId, card_Id: cardId})
        
        if(searchPractice == null){
            const dtPracticeTest = new PracticeTest(
                {
                    decksDetails_Id: decksDetailsId,
                    card_Id: cardId,
                    status: "Done",
                    type: false
                }
            );
            const result = await dtPracticeTest.save();
            
            const dtDecksDetails = await DecksDetails.findOne({_id: decksDetailsId})
            const idDecks = dtDecksDetails.decksId
            const scoreTrue = await PracticeTest.find({decksDetails_Id: decksDetailsId, type: "true"}).countDocuments()
            const scoreFalse = await PracticeTest.find({decksDetails_Id: decksDetailsId, type: "false"}).countDocuments()
            const findCard = await Card.find({decksId: idDecks}).countDocuments()
            const progress = (scoreTrue + scoreFalse) / findCard;
            const notStudied = findCard - (scoreTrue + scoreFalse)
            
            const dtUpdate = {
                userId: idUser,
                decksId: idDecks,
                progress: progress,
                true: scoreTrue,
                false: scoreFalse,
                notStudied: notStudied
            };
        
            dtDecksDetails.set(dtUpdate);
            const updateDecksDetails = await dtDecksDetails.save();
            

            res.status(200).json({
                statusCode: 200,
                statusText: 'success',
                message: " Your test practice successfully"
            });
        }else{
            const dtUpdatePractice = {
                decksDetails_Id: decksDetailsId,
                card_Id: cardId,
                status: "Done",
                type: "false"
            };
        
            searchPractice.set(dtUpdatePractice);
            const updatePractice = await searchPractice.save();
             
            const dtDecksDetails = await DecksDetails.findOne({_id: decksDetailsId})

            const idDecks = dtDecksDetails.decksId
            const scoreTrue = await PracticeTest.find({decksDetails_Id: decksDetailsId, type: "true"}).countDocuments()
            const scoreFalse = await PracticeTest.find({decksDetails_Id: decksDetailsId, type: "false"}).countDocuments()
            const findCard = await Card.find({decksId: idDecks}).countDocuments()
            const progress = (scoreTrue + scoreFalse) / findCard;
            const notStudied = findCard - (scoreTrue + scoreFalse)
            
            const dtUpdate = {
                userId: idUser,
                decksId: idDecks,
                progress: progress,
                true: scoreTrue,
                false: scoreFalse,
                notStudied: notStudied
            };
        
            dtDecksDetails.set(dtUpdate);
            const updateDecksDetails = await dtDecksDetails.save();

            res.status(200).json({
                statusCode: 200,
                statusText: 'success',
                message: " Your test practice successfully"
            });

        }
    }catch(error){
        res.status(500).send(error);
    }
}

method.swipeRight = async (req, res) => {
    try{
        const token = req.token;
        const idUser = token.id;
        const {decksDetailsId, cardId} = req.body

        const searchPractice = await PracticeTest.findOne({decksDetails_Id: decksDetailsId, card_Id: cardId})

        if(searchPractice == null){
            const dtPracticeTest = new PracticeTest(
                {
                    decksDetails_Id: decksDetailsId,
                    card_Id: cardId,
                    status: "Done",
                    type: true
                }
            );
            const result = await dtPracticeTest.save();

            const dtDecksDetails = await DecksDetails.findOne({_id: decksDetailsId})
            const idDecks = dtDecksDetails.decksId
            const scoreTrue = await PracticeTest.find({decksDetails_Id: decksDetailsId, type: "true"}).countDocuments()
            const scoreFalse = await PracticeTest.find({decksDetails_Id: decksDetailsId, type: "false"}).countDocuments()
            const findCard = await Card.find({decksId: idDecks}).countDocuments()
            const progress = (scoreTrue + scoreFalse) / findCard
            const notStudied = findCard - (scoreTrue + scoreFalse)

            const dtUpdate = {
                userId: idUser,
                decksId: idDecks,
                progress: progress,
                true: scoreTrue,
                false: scoreFalse,
                notStudied: notStudied
            };
        
            dtDecksDetails.set(dtUpdate);
            const updateDecksDetails = await dtDecksDetails.save();


            res.status(200).json({
                statusCode: 200,
                statusText: 'success',
                message: " Your test practice successfully"
            });
        }else{
            const dtUpdatePractice = {
                decksDetails_Id: decksDetailsId,
                card_Id: cardId,
                status: "Done",
                type: "true"
            };
        
            searchPractice.set(dtUpdatePractice);
            const updatePractice = await searchPractice.save();
             
            const dtDecksDetails = await DecksDetails.findOne({_id: decksDetailsId})

            const idDecks = dtDecksDetails.decksId
            const scoreTrue = await PracticeTest.find({decksDetails_Id: decksDetailsId, type: "true"}).countDocuments()
            const scoreFalse = await PracticeTest.find({decksDetails_Id: decksDetailsId, type: "false"}).countDocuments()
            const findCard = await Card.find({decksId: idDecks}).countDocuments()
            const progress = (scoreTrue + scoreFalse) / findCard;
            const notStudied = findCard - (scoreTrue + scoreFalse)
            
            const dtUpdate = {
                userId: idUser,
                decksId: idDecks,
                progress: progress,
                true: scoreTrue,
                false: scoreFalse,
                notStudied: notStudied
            };
        
            dtDecksDetails.set(dtUpdate);
            const updateDecksDetails = await dtDecksDetails.save();

            res.status(200).json({
                statusCode: 200,
                statusText: 'success',
                message: " Your test practice successfully"
            });
        }

    }catch(error){
        res.status(500).send(error);
    }
}

method.practiceRecall = async (req, res) => {
    try{
        const token = req.token;
        const idUser = token.id;
        const idDecks = req.params.id
        const searchDecksDetails = await DecksDetails.findOne({decksId: idDecks, userId: idUser})

        if(searchDecksDetails == null){
            const totalCardDecksId = await Card.find({ decksId: idDecks}).countDocuments();
            const dtDecksDetails = new DecksDetails(
                {
                    userId: idUser,
                    decksId: idDecks,
                    progress: 0,
                    true: 0,
                    false: 0,
                    notStudied: totalCardDecksId

                }
            );
            const result = await dtDecksDetails.save();
            const idDecksDet = dtDecksDetails._id

            const dtPracticeTest = await PracticeTest.find({decksDetails_Id: idDecksDet, type: "false"})
            const idCard = await dtPracticeTest.map((x) => x.card_Id)
            const dtCard = await Card.find({_id: {$in: idCard}, decksId: idDecks})

            res.status(200).json({
                statusCode: 200,
                statusText: 'success',
                message: " Your testing successfully",
                data:{
                    DecksDetails: result,
                    TestPractice: dtCard
                }
            });

        }else{
            const idDecksDet = searchDecksDetails._id
            const dtPracticeTest = await PracticeTest.find({decksDetails_Id: idDecksDet, type: "false"})
            const idCard = await dtPracticeTest.map((x) => x.card_Id)
            const dtCard = await Card.find({_id: {$in: idCard}, decksId: idDecks})

            res.status(200).json({
                statusCode: 200,
                statusText: 'success',
                message: " Your testing successfully",
                data:{
                    DecksDetails: searchDecksDetails,
                    TestPractice: dtCard
                }
            });
        }
    }catch(error){
        res.status(500).send(error);
    }
}

method.practiceMastered = async (req, res) => {
    try{
        const token = req.token;
        const idUser = token.id;
        const idDecks = req.params.id
        const searchDecksDetails = await DecksDetails.findOne({decksId: idDecks, userId: idUser})

        if(searchDecksDetails == null){
            const totalCardDecksId = await Card.find({ decksId: idDecks}).countDocuments();
            const dtDecksDetails = new DecksDetails(
                {
                    userId: idUser,
                    decksId: idDecks,
                    progress: 0,
                    true: 0,
                    false: 0,
                    notStudied: totalCardDecksId

                }
            );
            const result = await dtDecksDetails.save();
            const idDecksDet = dtDecksDetails._id

            const dtPracticeTest = await PracticeTest.find({decksDetails_Id: idDecksDet, type: "false"})
            const idCard = await dtPracticeTest.map((x) => x.card_Id)
            const dtCard = await Card.find({_id: {$in: idCard}, decksId: idDecks})

            res.status(200).json({
                statusCode: 200,
                statusText: 'success',
                message: " Your testing successfully",
                data:{
                    DecksDetails: result,
                    TestPractice: dtCard
                }
            });

        }else{
            const idDecksDet = searchDecksDetails._id
            const dtPracticeTest = await PracticeTest.find({decksDetails_Id: idDecksDet, type: "true"})
            
            const idCard = await dtPracticeTest.map((x) => x.card_Id)
            const dtCard = await Card.find({_id: {$in: idCard}, decksId: idDecks})

            res.status(200).json({
                statusCode: 200,
                statusText: 'success',
                message: " Your testing successfully",
                data:{
                    DecksDetails: searchDecksDetails,
                    TestPractice: dtCard
                }
            });
        }

    }catch(error){
        res.status(500).send(error);
    }
}



module.exports = method