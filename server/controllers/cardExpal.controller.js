const { CardExpal, Card, Deck, DecksTest, TakeTest} = require('../models');
const method = {};

method.getFormMultiply = async (req, res) => {
    try {
            const token = req.token;
            const idUser = token.id;
            const {decksId, cardId, status} = req.body;

            const dtDecksTest = await DecksTest.find({ userId: idUser, deckId: decksId });
            const totalCardDecksId = await Card.find({ decksId: decksId}).countDocuments();

            if(!cardId){
                //Jika IdCard tidak ada (Sebelum di HIT)
                const dtCard = await Card.find({ decksId: decksId}).skip(0).limit(1);

                if(dtCard[0] === undefined){
                    res.status(200).json({
                        statusCode: 200,
                        statusText: 'success',
                        message: "Finished",
                    });
                } else {
                    const dtDeck = await Deck.find({ _id: decksId }).select('title');
    
                    const dtIdCard = await dtCard.map((x) => x._id);
                    const IdCard = dtIdCard.toString();
    
                    const dtExplanation = await CardExpal.aggregate([
                        {$match: {cardId:IdCard}},
                        {$sample: {size: 4}}
                    ]);
    
                    res.status(200).json({
                        statusCode: 200,
                        statusText: 'success',
                        message: " Your request get Form Multiple successfully",
                        data: {totalCardDecksId, dtDeck, dtCard, dtExplanation}
                    });
                }
            } else {
                //Jika IdExplanation ada
                //create di tabel deckTest True
                if(status == "true"){
                    const score = (1 * 100) / totalCardDecksId;
                    const createDeckTest = new DecksTest({
                        userId: idUser,
                        deckId: decksId,
                        score: score,
                        true: 1,
                        false: 0
                    });

                    if(dtDecksTest[0] === undefined){
                        //jika belum ambil test (True)
                        const afterCreateDeckTest = await createDeckTest.save();

                        //create take_test 'A'
                        if(afterCreateDeckTest){
                            const getIddtDecksTest = await DecksTest.find({ userId: idUser, deckId: decksId });
                            const IdDecksTest = await getIddtDecksTest.map((x) => x._id);
                            
                            const postTakeTest = new TakeTest({
                                decksTest_Id: IdDecksTest,
                                cardId: cardId,
                                type: status
                            });

                            await postTakeTest.save();
                        }

                    } else {
                        //jika sudah ambil test (True)
                        const trueScore = await dtDecksTest.map((x) => x.true);
                        const ID = await dtDecksTest.map((y) => y._id);
                        const True = Number(trueScore) + 1;
                        const Score = (True * 100) / totalCardDecksId;

                        const idUpdate = await DecksTest.findById(ID);
                        const updateDeckTest = {
                            score: Score,
                            true: True,
                        }
                        
                        idUpdate.set(updateDeckTest);
                        const afterUpdateDecksTest = await idUpdate.save();

                        //post Take a test 'B'
                        if(afterUpdateDecksTest){
                            const postTakeTest = new TakeTest({
                                decksTest_Id: ID,
                                cardId: cardId,
                                type: status
                            });
                            await postTakeTest.save();
                        }

                    }

                } else {
                    //create di tabel deckTest False
                    const createDeckTest = new DecksTest({
                        userId: idUser,
                        deckId: decksId,
                        score: 0,
                        true: 0,
                        false: 1
                    });

                    if(dtDecksTest[0] === undefined){
                        //jika belum ambil test (False)
                        const afterCreateDeckTest = await createDeckTest.save();

                        //create take_test 'C'
                        if(afterCreateDeckTest){
                            const getIddtDecksTest = await DecksTest.find({ userId: idUser, deckId: decksId });
                            const IdDecksTest = await getIddtDecksTest.map((x) => x._id);
                            
                            const postTakeTest = new TakeTest({
                                decksTest_Id: IdDecksTest,
                                cardId: cardId,
                                type: status
                            });

                            await postTakeTest.save();
                        }

                    } else {
                        //jika sudah ambil test (False)
                        const falseMap = await dtDecksTest.map((x) => x.false);
                        const ID = await dtDecksTest.map((y) => y._id);
                        const False = Number(falseMap) + 1;

                        const idUpdate = await DecksTest.findById(ID);
                        const updateDeckTest = {
                            false: False
                        }
                        
                        idUpdate.set(updateDeckTest);
                        const afterUpdateDecksTest = await idUpdate.save();

                        //post Take a test 'D'
                        if(afterUpdateDecksTest){
                            const postTakeTest = new TakeTest({
                                decksTest_Id: ID,
                                cardId: cardId,
                                type: status
                            });
                            await postTakeTest.save();
                        }
                    }
                }
                
                const nextTrue = await dtDecksTest.map((x) => x.true);
                const nextFalse = await dtDecksTest.map((y) => y.false);
                const convToNumber = Number(nextTrue) + Number(nextFalse);

                //jika True False masih kosong
                if(convToNumber === 0){
                    const dtCard = await Card.find({ decksId: decksId}).skip(1).limit(1);
                
                    if(dtCard[0] === undefined){
                        res.status(200).json({
                            statusCode: 200,
                            statusText: 'success',
                            message: "Finished",
                        });
                    } else {

                        const dtDeck = await Deck.find({ _id: decksId }).select('title');
    
                        const dtIdCard = await dtCard.map((x) => x._id);
                        const IdCard = dtIdCard.toString();
    
                        const dtExplanation = await CardExpal.aggregate([
                            {$match: {cardId:IdCard}},
                            {$sample: {size: 4}}
                        ]);

                        res.status(200).json({
                            statusCode: 200,
                            statusText: 'success',
                            message: " Your request get Form Multiple successfully",
                            data: {dtDeck, dtCard, dtExplanation}
                        });
                    }
                } else {

                        // res.json(totalCardDecksId);
                        if(convToNumber < totalCardDecksId){
                            const Next = convToNumber + 1;

                            const dtCard = await Card.find({ decksId: decksId}).skip(Next).limit(1);
                
                            if(dtCard[0] === undefined){
                                res.status(200).json({
                                statusCode: 200,
                                statusText: 'success',
                                message: "Finished",
                            });
                            } else {

                            const dtDeck = await Deck.find({ _id: decksId }).select('title');
    
                            const dtIdCard = await dtCard.map((x) => x._id);
                            const IdCard = dtIdCard.toString();
    
                            const dtExplanation = await CardExpal.aggregate([
                                {$match: {cardId:IdCard}},
                                {$sample: {size: 4}}
                            ]);

                            res.status(200).json({
                                statusCode: 200,
                                statusText: 'success',
                                message: " Your request get Form Multiple successfully",
                                data: {dtDeck, dtCard, dtExplanation}
                            });
                        }
                    } else {
                        res.status(204).json({
                            statusCode: 204,
                            statusText: 'success',
                            message: " Your Take a test is finish!!",
                        });
                    }
                }
            }

        } catch (error) {
            res.status(500).send(error);
        }
}

method.getFormTrueFalse = async (req, res) => {
    try {
        const token = req.token;
        const idUser = token.id;
        const {decksId, cardId, status, type} = req.body;
        const defaultTrue = true;
        const defaultFalse = false;

        const dtDecksTest = await DecksTest.find({ userId: idUser, deckId: decksId });
        const totalCardDecksId = await Card.find({ decksId: decksId}).countDocuments();

        if(!cardId){
            //Jika IdCard tidak ada (Sebelum di HIT)
            const dtCard = await Card.find({ decksId: decksId}).skip(0).limit(1);

            if(dtCard[0] === undefined){
                res.status(200).json({
                    statusCode: 200,
                    statusText: 'success',
                    message: "Finished",
                });
            } else {
                const dtDeck = await Deck.find({ _id: decksId }).select('title');

                const dtIdCard = await dtCard.map((x) => x._id);
                const IdCard = dtIdCard.toString();

                const dtExplanation = await CardExpal.aggregate([
                    {$match: {cardId:IdCard}},
                    {$sample: {size: 4}}
                ]).limit(1);

                res.status(200).json({
                    statusCode: 200,
                    statusText: 'success',
                    message: " Your request get Form Multiple successfully",
                    data: {totalCardDecksId, dtDeck, dtCard, dtExplanation}
                });
            }
        } else {
            //Jika IdExplanation ada
            //create di tabel deckTest True
            if(status == "true" && type == "true" || status == "false" && type == "false"){
                const score = (1 * 100) / totalCardDecksId;
                const createDeckTest = new DecksTest({
                    userId: idUser,
                    deckId: decksId,
                    score: score,
                    true: 1,
                    false: 0
                });

                if(dtDecksTest[0] === undefined){
                    //jika belum ambil test (True)
                    const afterCreateDeckTest = await createDeckTest.save();

                    //create take_test 'A'
                    if(afterCreateDeckTest){
                        const getIddtDecksTest = await DecksTest.find({ userId: idUser, deckId: decksId });
                        const IdDecksTest = await getIddtDecksTest.map((x) => x._id);
                        
                        const postTakeTest = new TakeTest({
                            decksTest_Id: IdDecksTest,
                            cardId: cardId,
                            type: defaultTrue
                        });
                        await postTakeTest.save();
                    }

                } else {
                    //jika sudah ambil test (True)
                    const trueScore = await dtDecksTest.map((x) => x.true);
                    const ID = await dtDecksTest.map((y) => y._id);
                    const True = Number(trueScore) + 1;
                    const Score = (True * 100) / totalCardDecksId;

                    const idUpdate = await DecksTest.findById(ID);
                    const updateDeckTest = {
                        score: Score,
                        true: True,
                    }
                    
                    idUpdate.set(updateDeckTest);
                    const afterUpdateDecksTest = await idUpdate.save();

                    //post Take a test 'B'
                    if(afterUpdateDecksTest){
                        const postTakeTest = new TakeTest({
                            decksTest_Id: ID,
                            cardId: cardId,
                            type: defaultTrue
                        });
                        await postTakeTest.save();
                    }
                }

            } else {
                //create di tabel deckTest False
                const createDeckTest = new DecksTest({
                    userId: idUser,
                    deckId: decksId,
                    score: 0,
                    true: 0,
                    false: 1
                });

                if(dtDecksTest[0] === undefined){
                    //jika belum ambil test (False)
                    const afterCreateDeckTest = await createDeckTest.save();

                    //create take_test 'C'
                    if(afterCreateDeckTest){
                        const getIddtDecksTest = await DecksTest.find({ userId: idUser, deckId: decksId });
                        const IdDecksTest = await getIddtDecksTest.map((x) => x._id);
                        
                        const postTakeTest = new TakeTest({
                            decksTest_Id: IdDecksTest,
                            cardId: cardId,
                            type: defaultFalse
                        });
                        await postTakeTest.save();
                    }

                } else {
                    //jika sudah ambil test (False)
                    const falseMap = await dtDecksTest.map((x) => x.false);
                    const ID = await dtDecksTest.map((y) => y._id);
                    const False = Number(falseMap) + 1;

                    const idUpdate = await DecksTest.findById(ID);
                    const updateDeckTest = {
                        false: False
                    }
                    
                    idUpdate.set(updateDeckTest);
                    const afterUpdateDecksTest = await idUpdate.save();

                    //post Take a test 'D'
                    if(afterUpdateDecksTest){
                        const postTakeTest = new TakeTest({
                            decksTest_Id: ID,
                            cardId: cardId,
                            type: defaultFalse
                        });
                        await postTakeTest.save();
                    }
                }
            }
            
            const nextTrue = await dtDecksTest.map((x) => x.true);
            const nextFalse = await dtDecksTest.map((y) => y.false);
            const convToNumber = Number(nextTrue) + Number(nextFalse);

            //jika True False masih kosong
            if(convToNumber === 0){
                const dtCard = await Card.find({ decksId: decksId}).skip(1).limit(1);
            
                if(dtCard[0] === undefined){
                    res.status(200).json({
                        statusCode: 200,
                        statusText: 'success',
                        message: "Finished",
                    });
                } else {

                    const dtDeck = await Deck.find({ _id: decksId }).select('title');

                    const dtIdCard = await dtCard.map((x) => x._id);
                    const IdCard = dtIdCard.toString();

                    const dtExplanation = await CardExpal.aggregate([
                        {$match: {cardId:IdCard}},
                        {$sample: {size: 4}}
                    ]).limit(1);

                    res.status(200).json({
                        statusCode: 200,
                        statusText: 'success',
                        message: " Your request get Form Multiple successfully",
                        data: {dtDeck, dtCard, dtExplanation}
                    });
                }
            } else {

                    // res.json(totalCardDecksId);
                    if(convToNumber < totalCardDecksId){
                        const Next = convToNumber + 1;

                        const dtCard = await Card.find({ decksId: decksId}).skip(Next).limit(1);
            
                        if(dtCard[0] === undefined){
                            res.status(200).json({
                            statusCode: 200,
                            statusText: 'success',
                            message: "Finished",
                        });
                        } else {

                        const dtDeck = await Deck.find({ _id: decksId }).select('title');

                        const dtIdCard = await dtCard.map((x) => x._id);
                        const IdCard = dtIdCard.toString();

                        const dtExplanation = await CardExpal.aggregate([
                            {$match: {cardId:IdCard}},
                            {$sample: {size: 4}}
                        ]).limit(1);

                        res.status(200).json({
                            statusCode: 200,
                            statusText: 'success',
                            message: " Your request get Form Multiple successfully",
                            data: {dtDeck, dtCard, dtExplanation}
                        });
                    }
                } else {
                    res.status(204).json({
                        statusCode: 204,
                        statusText: 'success',
                        message: " Your Take a test is finish!!",
                    });
                }
            }
        }

    } catch (error) {
        res.status(500).send(error);
    }
}

method.cancelTakeTest = async (req, res) => {
    try {
        const token = req.token;
        const idUser = token.id;
        const checkIdDecksTest = await DecksTest.find({ userId: idUser, deckId: req.params.id });

        if (checkIdDecksTest) {
            const dtIdDecksTest = await checkIdDecksTest.map((x) => x._id);

            await DecksTest.deleteOne({ _id: dtIdDecksTest });
            await TakeTest.deleteMany({ decksTest_Id: dtIdDecksTest });

            res.status(200).send({
                statusCode: 200,
                statusText: "success",
                message: " Cancel Take a test Succesfully "
            });
      } else {
        res.status(400).send({ message: "id does not exist" });
      }
    } catch (error) {
        res.status(500).send(error);
    }
}

method.delCreate = async (req, res) => {
    try {
        const {cardId, explane1, explane2, explane3, explane4} = req.body;

    const createExplane1 = new CardExpal({
        cardId: cardId, 
        explane1: explane1, 
        status: true
    });

    const createExplane2 = new CardExpal({
        cardId: cardId, 
        explane2: explane2,
        status: false
    });

    const createExplane3 = new CardExpal({
        cardId: cardId, 
        explane3: explane3,
        status: false
    });

    const createExplane4 = new CardExpal({
        cardId: cardId, 
        explane4: explane4,
        status: false
    });

    await createExplane1.save();
    await createExplane2.save();
    await createExplane3.save();
    await createExplane4.save();
    
        res.send({
            statusCode: 200,
            statusText: "success",
            message: " Your request for Create CardExpal successfully",
        });   
    } catch (error) {
        res.status(500).send(error);
    }
}

module.exports = method;