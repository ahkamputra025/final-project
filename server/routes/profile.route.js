const express = require('express');
const router = express.Router();

const {getProfile, putProfile, putPassword} = require('../controllers/profile.controller');
const {checkToken, validateProfile, validatePassword} = require('../middleware/auth.middleware');

router.get('/getProfile', checkToken, getProfile);
router.put('/putProfile', validateProfile, checkToken, putProfile);
router.put('/putPassword', validatePassword, checkToken, putPassword);

module.exports = router;