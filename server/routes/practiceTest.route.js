const express = require("express");
const router = express.Router();

const {practiceTest, swipeLeft, swipeRight, practiceRecall, practiceMastered} = require('../controllers/practiceTest.controller')
const {checkToken} = require('../middleware/auth.middleware');


router.get('/practiceTest/:id', checkToken, practiceTest);
router.post('/swipeLeft', checkToken, swipeLeft);
router.post('/swipeRight', checkToken, swipeRight);
router.get('/recall/:id', checkToken, practiceRecall);
router.get('/mastered/:id', checkToken, practiceMastered);

module.exports = router;