const express = require('express');
const router = express.Router();

const controllers = require('../controllers/auth.controllers');
const middleware = require('../middleware/auth.middleware');

router.post('/register', middleware.ValidateRegister, controllers.postRegistrasi);
router.post('/login', middleware.ValidateLogin, controllers.postLogin);


module.exports = router;