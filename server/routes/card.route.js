const express = require('express');
const router = express.Router();
const upload = require('../service/multer')

const {createCard, updateCard, deleteCard, getAllCard, getCardId} = require('../controllers/card.controller')
const {checkToken, validateCreateorUpdateCard, validateCreateorUpdateDeck } = require('../middleware/auth.middleware');

router.get('/getAllCard', checkToken, getAllCard)
router.get('/getCard/:decksId', checkToken, getCardId)
router.post('/createCard', upload.single('image'), validateCreateorUpdateCard, checkToken, createCard)
router.put('/updateCard/:id',  upload.single('image'), validateCreateorUpdateCard, checkToken, updateCard)
router.delete('/deleteCard/:id', checkToken, deleteCard)

module.exports = router