const express = require('express');
const router = express.Router();

const {getdecksDetailsByIdDecks, getDecksDetailsAllByUserID, createDecksDetails, putDecksDetails} = require('../controllers/deckDetails.controller');
const {checkToken} = require('../middleware/auth.middleware');

router.get('/getdecksDetailsByIdDecks/:id', checkToken, getdecksDetailsByIdDecks);
router.get('/getDecksDetailsAllByUserID/', checkToken, getDecksDetailsAllByUserID);
router.post('/postDecksDetails', createDecksDetails);
router.put('/putDecksDetails/:id', putDecksDetails);

module.exports = router;