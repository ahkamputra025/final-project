const express = require('express');
const router = express.Router();

const {getResultTakeTest, RetakeTheTest} = require('../controllers/take_test.controller');
const {checkToken} = require('../middleware/auth.middleware');

router.get('/getResultTakeTest/:id', checkToken, getResultTakeTest);
router.put('/RetakeTheTest/:id', checkToken, RetakeTheTest);

module.exports = router;